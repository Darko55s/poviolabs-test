//
//  Utils.h
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface Utils : NSObject <MBProgressHUDDelegate>

+ (void)showActivityViewFrom:(UIView*)view;
+ (void)showActivityViewFrom:(UIView*)view withText:(NSString*)text;
+ (void)hideActivityViewFrom:(UIView*)view;

@end
