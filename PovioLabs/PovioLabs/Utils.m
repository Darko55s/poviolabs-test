//
//  Utils.m
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import "Utils.h"

#define ACTIVITY_INDICATOR_TAG 98756

@implementation Utils

+ (void)showActivityViewFrom:(UIView*)view{
    MBProgressHUD *Hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:Hud];
    
    Hud.tag = ACTIVITY_INDICATOR_TAG;
    Hud.labelText = @"Loading...";
    [Hud show:YES];
}


+ (void)showActivityViewFrom:(UIView*)view withText:(NSString*)text{
    MBProgressHUD *Hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:Hud];
    
    Hud.tag = ACTIVITY_INDICATOR_TAG;
    Hud.labelText = text;
    [Hud show:YES];
}


+ (void)hideActivityViewFrom:(UIView*)view{
    MBProgressHUD *activity = (MBProgressHUD*)[view viewWithTag:ACTIVITY_INDICATOR_TAG];
    // [activity animateActivityIndicator:NO];
    [activity removeFromSuperview];
    
}

@end
