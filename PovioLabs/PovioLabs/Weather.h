//
//  Weather.h
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

@property (nonatomic, copy) NSString *weatherId;
@property (nonatomic, copy) NSString *weatherDescription;
@property (nonatomic, copy) NSString *main;
@property (nonatomic, assign) double curentTemp;
@property (nonatomic, assign) double pressure;
@property (nonatomic, assign) double humidity;

@end
