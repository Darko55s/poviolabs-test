//
//  DataModel.m
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import "DataModel.h"

static DataModel *_model = nil;

@implementation DataModel

+(DataModel*)model{
    @synchronized([DataModel class]){
        if (!_model) {
            _model = [[self alloc] init];
            _model.cities = [[NSMutableArray alloc] init];
            [_model getCitiesFromUserDefaults];
        }
        return _model;
    }
    return nil;
}

- (City*)parseSelectedCityResponse:(NSDictionary*)jsonResponse{
    City *city = [[City alloc] init];
    city.cityId = [[jsonResponse objectForKey:@"id"] stringValue];
    city.name = [jsonResponse objectForKey:@"name"];
    city.weather = [[Weather alloc] init];
    city.weather.curentTemp = [[[jsonResponse objectForKey:@"main"] objectForKey:@"temp"] doubleValue];
    city.weather.pressure = [[[jsonResponse objectForKey:@"main"] objectForKey:@"pressure"] doubleValue];
    city.weather.humidity = [[[jsonResponse objectForKey:@"main"] objectForKey:@"humidity"] doubleValue];
    city.weather.weatherDescription = [[[jsonResponse objectForKey:@"weather"] objectAtIndex:0] objectForKey:@"description"];
    return city;
}

- (void)addCityWithName:(NSString*)cityName{
    [self.cities addObject:cityName];
    [self saveCitiesInUserDefaults];
}

- (void)removeCityWithName:(NSString*)cityName{
    NSUInteger indexOfDeletingItem = -1;;
    for (NSString *name in self.cities) {
        if ([cityName isEqualToString:name]) {
            indexOfDeletingItem = [self.cities indexOfObject:name];
            break;
        }
    }
    
    if (indexOfDeletingItem!=-1) {
        [self.cities removeObjectAtIndex:indexOfDeletingItem];
    }
    [self saveCitiesInUserDefaults];
}

- (void)saveCitiesInUserDefaults{
    [[NSUserDefaults standardUserDefaults] setObject:self.cities forKey:@"Cities"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)getCitiesFromUserDefaults{
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:@"Cities"];
    self.cities = [array mutableCopy];
}

@end
