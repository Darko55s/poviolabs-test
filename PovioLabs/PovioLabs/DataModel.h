//
//  DataModel.h
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "City.h"


@interface DataModel : NSObject

+(DataModel*)model;

@property (nonatomic, retain) NSMutableArray *cities;


- (City*)parseSelectedCityResponse:(NSDictionary*)jsonResponse;
- (void)addCityWithName:(NSString*)cityName;
- (void)removeCityWithName:(NSString*)cityName;

- (void)saveCitiesInUserDefaults;
- (void)getCitiesFromUserDefaults;

@end
