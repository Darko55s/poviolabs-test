//
//  ServiceCaller.h
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionBlock)(NSDictionary* result, NSError *error);

@interface ServiceCaller : NSObject

+(ServiceCaller*)callService;

- (void)getWeatherInfoForSelectedCity:(NSString*)cityName withCompletion:(CompletionBlock)completion;
@end
