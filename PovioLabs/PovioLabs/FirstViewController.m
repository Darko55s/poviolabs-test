//
//  ViewController.m
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import "FirstViewController.h"
#import "DataModel.h"
#import "ServiceCaller.h"
#import "Utils.h"
#import "ThirdViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRefreshControl];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark Actions
- (IBAction)onAdd:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"addCitySegue" sender:nil];
}

#pragma mark - 
#pragma mark UITableView Delegate & DataSource
//cityDetailsSegue
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [DataModel model].cities.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    NSString *cityTitle = [[DataModel model].cities objectAtIndex:indexPath.row];
    cell.textLabel.text = cityTitle;
    if ([cell.contentView viewWithTag:1010]) {
        [[cell.contentView viewWithTag:1010] removeFromSuperview];
    }
    UILabel *label = [[UILabel alloc ]initWithFrame:CGRectMake(cell.frame.size.width-120, 0, 120, cell.frame.size.height)];
    [cell.contentView addSubview:label];
    label.tag = 1010;
    [[ServiceCaller callService] getWeatherInfoForSelectedCity:cityTitle withCompletion:^(NSDictionary *result, NSError *error) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error) {
                City *selectedCity = [[DataModel model] parseSelectedCityResponse:result];
                label.text = [NSString stringWithFormat:@"Temp: %.02f",selectedCity.weather.curentTemp];
            }
        });
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *cityTitle = [[DataModel model].cities objectAtIndex:indexPath.row];

    if (cityTitle.length > 0) {
        [Utils showActivityViewFrom:self.view];
        [[ServiceCaller callService] getWeatherInfoForSelectedCity:cityTitle withCompletion:^(NSDictionary *result, NSError *error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (!error) {
                        City *selectedCity = [[DataModel model] parseSelectedCityResponse:result];
                        [self performSegueWithIdentifier:@"cityDetailsSegue" sender:selectedCity];
                   
                }else{
                    [self showInvalidCityAlert];
                }
                [Utils hideActivityViewFrom:self.view];
            });
        }];
    }else{
        [self showInvalidCityAlert];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        NSString *cityTitle = [[DataModel model].cities objectAtIndex:indexPath.row];
        [[DataModel model] removeCityWithName:cityTitle];
        [self.tableView reloadData];
    }
}

#pragma mark - 
#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"cityDetailsSegue"]) {
        ThirdViewController *thirdViewController = (ThirdViewController*)[segue destinationViewController];
        thirdViewController.selectedCity = (City*)sender;
    }
}

#pragma mark - 
#pragma mark - Private

- (void)showInvalidCityAlert{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"Invalid City"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)addRefreshControl{
    self.refreshControll = [[UIRefreshControl alloc] init];
    self.refreshControll.tintColor = [UIColor blackColor];
    [self.refreshControll addTarget:self
                            action:@selector(refreshContent:)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControll];
}

- (void)refreshContent:(UIRefreshControl*)refreshControll{
    if ([self.refreshControll  isRefreshing]) {
        [self.tableView reloadData];
        [self.refreshControll endRefreshing];
    }
}

@end
