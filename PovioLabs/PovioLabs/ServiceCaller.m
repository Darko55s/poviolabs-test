//
//  ServiceCaller.m
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import "ServiceCaller.h"
#define kBaseAPIUrl @"http://api.openweathermap.org/"
#define kWeatherAPIKey @"dcc7704e0fcb8e0b7ab3c6e45805fc8e"


static ServiceCaller *_callService = nil;

@implementation ServiceCaller

+(ServiceCaller*)callService{
    @synchronized([ServiceCaller class]){
        if (!_callService) {
            _callService = [[self alloc] init];
        }
        return _callService;
    }
    return nil;
}

+(id)alloc{
    @synchronized([ServiceCaller class]){
        NSAssert(_callService==nil, @"Attempted to allocate a second instance of a singleton");
        _callService = [super alloc];
        return _callService;
    }
    return nil;
}

- (void)getWeatherInfoForSelectedCity:(NSString*)cityName withCompletion:(CompletionBlock)completion{
    NSString *urlString = [NSString stringWithFormat:@"%@data/2.5/weather?q=%@&appid=%@",kBaseAPIUrl,cityName,kWeatherAPIKey];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@" application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSession *session =[NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSError *parsingError = nil;
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parsingError];
            if (!parsingError && jsonResponse) {
                completion(jsonResponse,nil);
            }else{
                completion(nil,error);
            }
        }else{
            completion(nil,error);
        }
    }]resume];
}


//http://openweathermap.org/data/2.5/weather?q=London&appid=dcc7704e0fcb8e0b7ab3c6e45805fc8e
//http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=2de143494c0b295cca9337e1e96b00e0

@end
