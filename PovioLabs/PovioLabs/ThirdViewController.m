//
//  ThirdViewController.m
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setValuesInLabels];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setValuesInLabels{
    self.lblCityName.text = self.selectedCity.name;
    self.lblDescription.text = self.selectedCity.weather.weatherDescription;
    self.lblHumidity.text = [NSString stringWithFormat:@"%.02f",self.selectedCity.weather.humidity];
    self.lblTemperature.text = [NSString stringWithFormat:@"%.02f",self.selectedCity.weather.curentTemp];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
