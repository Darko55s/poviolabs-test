//
//  City.h
//  PovioLabs
//
//  Created by Darko Spasovski on 11/10/15.
//  Copyright © 2015 Darko Spasovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weather.h"

@interface City : NSObject

@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, retain) Weather *weather;

@end
